# Acquisitions API Quick Start Guide Code Example

# acq.rb - The purpose of this software is to illustrate some of the concepts
# described in the Acquisitions API Quick Start Guide.  It follows a
# "happy path" and is structured out of convenience and has no error handling.

# Written in 2017 by Milton Howard, mhoward@iii.com
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain worldwide.
# This software is distributed without any warranty. You should have received a copy
# of the CC0 Public Domain Dedication along with this software in the file LICENSE.txt.
# If not, see http://creativecommons.org/publicdomain/zero/1.0.

# In the directory for your project, install the following GEMs
#   gem install base64               Base64 Hex Encoding/Decoding
#   gem install json                 Converts Ruby objects to JSON
#   gem install rest-client          REST client library
#   gem install faraday              REST client library
#   gem install faraday_middleware   REST client library middleware

require 'base64'
require 'json'
require 'rest-client'
require 'faraday'
require 'faraday_middleware'


############# Set Up Info for Sierra API Host #############

# Sierra API set-up info - enter your API key and secret
SIERRA_API_HOST = 'https://sandbox.iii.com'  # Hostname for Sierra API
SIERRA_API_KEY = '<your API key>'                       # API key for host
SIERRA_API_KEY_SECRET = '<your API secret>'           # API secret for host

# Set URI for auth endpoint
SIERRA_AUTH_URI = '/iii/sierra-api/v3/token'

# Set grant type request headers
REQUEST_BODY = {
  :grant_type => 'client_credentials',  # Request a client credentials grant authorization
}

# Make the call to the Auth endpoint to get a bearer token

auth_response = RestClient.post("#{SIERRA_API_HOST}#{SIERRA_AUTH_URI}",
                          REQUEST_BODY.to_json,    # Encode the entire body as JSON
                          {:authorization => "Basic #{Base64.strict_encode64("#{SIERRA_API_KEY}:#{SIERRA_API_KEY_SECRET}")}",
                           :content_type => 'application/x-www-form-urlencoded',
                           :accept => 'application/json'})

puts "********** RESPONSE to POST #{SIERRA_API_HOST}#{SIERRA_AUTH_URI} **********"   

http_response_code = auth_response.code #HTTP response code
puts "Response code: #{http_response_code}" # Print to stdout

access_token = JSON.parse(auth_response.body)["access_token"]  # Squirrel away the access token

# Create a connection object (encoding, auth header, content-type) to Sierra API to be used
# to make subsequent calls to the API
conn = Faraday.new(:url => SIERRA_API_HOST) do |faraday|
  faraday.request  :url_encoded             # URL-encode POST params
  faraday.response :json                    # set response format to JSON
  faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
  faraday.headers['Authorization'] = "Bearer #{access_token}"
  faraday.headers['Content-Type'] = 'application/json'
end

order_params = '{
    "login": "acqstaff1",
    "copies":10,
    "allocation": [
      {
        "location": "00",
        "fund": "acfic",
         "copies": 4
      },
      {
        "location": "00aca",
        "fund": "acnf",
         "copies": 3
      },
      {
        "location": "00ar",
        "fund": "jcpb",
         "copies": 3
      }
    ],

    "vendor": "vend1" 
}'

# Validate the order parameters before placing the order
validate = conn.post do |req|
  req.url '/iii/sierra-api/v3/acquisitions/orders/validate'
   req.body = order_params
end

puts "********** RESPONSE to POST /v3/acquisitions/orders/validate **********"

http_response_code = validate.status # HTTP status code
puts "Response code: #{http_response_code}" # Print to stdout

# Ordinarily, the user interface for your ordering software would enable a user to
# dynamically select the following order parameters and bib information.  We've
# statically set the information for this example.
order_params_and_bib = '{
  "order": {
    "login": "acqstaff1",
    "copies":10,
    "allocation": [
      {
    "location":"00",
    "fund":"acfic",
    "copies": 4
  },
  {
    "location":"00aca",
    "fund":"acnf",
    "copies": 3
  },
  {
    "location":"00ar",
    "fund":"jcpb",
    "copies": 3
  }
    ],

    "vendor": "vend1"
  },
  "marcContentType": "application/marc-in-json",
  "marc": {
    "leader": "00000nam a2200000 a 4500",
    "fields": [{
      "001": "8739"
    }, {
      "008": "690612s1969    caua     b    001 0 eng  cam   "
    }, {
      "100": {
        "subfields": [{
          "a": "Albrecht, Bob,"
        }, {
          "d": "1930-"
        }],
        "ind1": "1",
        "ind2": " "
      }
    }, {
      "700": {
        "subfields": [{
          "a": "Lindberg, Eric."
        }],
        "ind1": "1",
        "ind2": " "
      }
    }, {
      "700": {
        "subfields": [{
          "a": "Mara, Walter."
        }],
        "ind1": "1",
        "ind2": " "
      }
    }, {
      "650": {
        "subfields": [{
          "a": "Mathematics"
        }, {
          "x": "Data processing."
        }],
        "ind1": " ",
        "ind2": "0"
      }
    }, {
      "650": {
        "subfields": [{
          "a": "Computers."
        }],
        "ind1": " ",
        "ind2": "0"
      }
    }, {
      "650": {
        "subfields": [{
          "a": "Programming languages (Electronic computers)"
        }],
        "ind1": " ",
        "ind2": "0"
      }
    }, {
      "010": {
        "subfields": [{
          "a": "77004256"
        }],
        "ind1": " ",
        "ind2": " "
      }
    }, {
      "504": {
        "subfields": [{
          "a": "Bibliography: p. 202."
        }],
        "ind1": " ",
        "ind2": " "
      }
    }, {
      "967": {
        "subfields": [{
          "a": "ib10001207"
        }],
        "ind1": " ",
        "ind2": " "
      }
    }, {
      "260": {
        "subfields": [{
          "a": "Menlo Park, Calif.,"
        }, {
          "b": "Addison-Wesley Pub. Co."
        }, {
          "c": "[1969]"
        }],
        "ind1": " ",
        "ind2": " "
      }
    }, {
      "300": {
        "subfields": [{
          "a": "204 p."
        }, {
          "b": "illus."
        }, {
          "c": "28 cm."
        }],
        "ind1": " ",
        "ind2": " "
      }
    }, {
      "245": {
        "subfields": [{
          "a": "Computer methods in mathematics"
        }, {
          "c": "[by] Robert L. Albrecht, Eric Lindberg [and] Walter Mara."
        }],
        "ind1": "1",
        "ind2": "0"
      }
    }, {
      "040": {
        "subfields": [{
          "a": "DLC"
        }, {
          "c": "DLC"
        }, {
          "d": "CSJ"
        }],
        "ind1": " ",
        "ind2": " "
      }
    }, {
      "049": {
        "subfields": [{
          "a": "CSJS"
        }],
        "ind1": " ",
        "ind2": " "
      }
    }, {
      "050": {
        "subfields": [{
          "a": "QA76.5"
        }, {
          "b": ".A368"
        }],
        "ind1": " ",
        "ind2": " "
      }
    }, {
      "910": {
        "subfields": [{
          "a": "eng"
        }, {
          "b": "901010"
        }, {
          "c": "m"
        }, {
          "d": "a"
        }, {
          "e": "cau"
        }],
        "ind1": " ",
        "ind2": " "
      }
    }, {
      "910": {
        "subfields": [{
          "a": "12APR78rev LB 8739"
        }],
        "ind1": " ",
        "ind2": " "
      }
    }]
  }
}'

# Make the call to the POST /v3/acquisitions/orders endpoint.
order = conn.post do |req|
  req.url '/iii/sierra-api/v3/acquisitions/orders'
  req.body = order_params_and_bib
end

puts "********** RESPONSE to POST /v3/acquisitions/orders **********"

http_response_code = order.status # HTTP status code
puts "Response code: #{http_response_code}" # Print to stdout

controlNumber = order.body["controlNumber"] # Parse out controlNumber from HTTP body response
puts "controlNumber: #{controlNumber}" # Print to stdout

orderId = order.body["orderId"] # Parse out orderId from HTTP body response
puts "orderId Link: #{orderId}" # Print to stdout

bibId = order.body["bibId"] # Parse out bibId from HTTP body response
puts "bibId Link: #{bibId}" # Print to stdout

legacyOrderId = order.body["legacyOrderId"] # Parse out legacyOrderId from HTTP body response
puts "legacyOrderId: #{legacyOrderId}" # Print to stdout
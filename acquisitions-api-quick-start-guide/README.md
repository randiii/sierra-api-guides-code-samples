# Acquisitions API Quick Start Guide

The purpose of the guide is to assist the experienced software developer who wants to quickly come up to speed on how to use the Sierra acquisitions API to create a better and more streamlined ordering experience for Sierra library staff.

# Contents

* Acquisitions API Quick Start Guide.pdf
* acq.rb - Sample code, written in Ruby, for authenticating and placing an order.
# Sierra API Code Samples

The purpose of this repository is to share code samples developed by Innovative to demonstrate the Sierra API.  These code samples are available for you to use under the license terms as described in the source code.

We hope these samples are helpful in getting you up to speed quickly on how to use the APIs available on our platforms.

# Where is the Code?
On the left-side navigation, click on "Source".
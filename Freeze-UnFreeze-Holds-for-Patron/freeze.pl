#!/usr/bin/perl

# Freeze or unfreeze holds for a patron

# freeze.pl - The purpose of this software is to illustrate some of the concepts
# for freezing and unfreezing holds using the Sierra API.  It follows a
# "happy path" and is structured out of convenience and has no error handling.

# Written in 2016 by Laura Frasca, Innovative Interfaces
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain worldwide.
# This software is distributed without any warranty. You should have received a copy
# of the CC0 Public Domain Dedication along with this software in the file LICENSE.txt.
# If not, see http://creativecommons.org/publicdomain/zero/1.0.

# Usage:
# Freeze holds for p1000001: freeze.pl --patron 1000001
# Unfreeze holds for p1000001: freeze.pl --patron 1000001 -u

use strict;
use warnings;

use Getopt::Long; # access command line options
use MIME::Base64; # base64 encoding for key and secret
use REST::Client;
use JSON; # encode and decode JSON
use Data::Dumper; # prints JSON response as perl structures, for dev only

##############################################################################
# declare variables

# set up client
my $server = 'buck-app.iii.com';
my $key = '<<Enter your API key here>>';
my $secret = '<<Enter your API secret here>>';
my $base_url = 'https://' . $server . '/iii/sierra-api';
my $client = REST::Client->new();

my $default_headers = {};

my $freeze = 1;
my $patron = '';

##############################################################################
# grab command line options

#Getopt::Long::Configure ("bundling");

GetOptions(
	'freeze|f' => \$freeze,
	'unfreeze|u' => sub { $freeze = 0 },
	'patron|p=s' => \$patron
);

die "No patron specified.  Aborting.\n" if ($patron !~ /^[0-9]*$/);


##############################################################################
# get_access_token: retrieve authentication token
# returns access token

sub get_access_token {
	my $url = $base_url . '/v2/token';
	my $headers = {
		Accept => 'application/json',
		Authorization => 'Basic ' . encode_base64($key . ':' . $secret)
	};
	my $body_content = 'grant_type=client_credentials';

	$client->POST($url, $body_content, $headers);
	my $response = decode_json($client->responseContent());

	$default_headers = {
    	Accept => 'application/json',
    	Authorization => 'Bearer ' . $response->{access_token}
	};
}

##############################################################################
# is_hold_frozen: return 1 if hold is frozen, 0 if hold is not frozen

sub is_hold_frozen {
	my ($hold_url) = @_;

	$client->GET($hold_url . '?fields=frozen', $default_headers);
	my $response = decode_json($client->responseContent());

	#print Dumper($client->responseContent());
	return $response->{frozen};
}

##############################################################################
# freeze_hold, unfreeze_hold

sub freeze_hold {
	my ($hold_url) = @_;

	my $body_content = '{ "freeze" : true }';

	$client->PUT($hold_url, $body_content, $default_headers);

	if ($client->responseCode() == 200) {
		printf "Froze %s\n", $hold_url;
	}
}

sub unfreeze_hold {
	my ($hold_url) = @_;

	my $body_content = '{ "freeze" : false }';

	$client->PUT($hold_url, $body_content, $default_headers);

	if ($client->responseCode() == 200) {
		printf "Unfroze %s\n", $hold_url;
	}
}

##############################################################################
# freeze_patron_holds: freeze holds for a patron

sub freeze_patron_holds {
	my ($patron_url) = @_;

	$client->GET($patron_url . '?fields=frozen', $default_headers);
	my $response = decode_json($client->responseContent());

	#print Dumper($client->responseContent());
	#print Dumper($response->{entries});

    foreach my $i (@{$response->{entries}}) {
		#printf ("%s %s\n", $i->{id}, $i->{frozen});
		if ($i->{frozen}) {
			printf "WARN: %s is already frozen\n", $i->{id};
		} else {
			&freeze_hold($i->{id});
		}
	}

}

sub unfreeze_patron_holds {
	my ($patron_url) = @_;

	$client->GET($patron_url . '?fields=frozen', $default_headers);
	my $response = decode_json($client->responseContent());

	#print Dumper($client->responseContent());
	#print Dumper($response->{entries});

    foreach my $i (@{$response->{entries}}) {
		#printf ("%s %s\n", $i->{id}, $i->{frozen});
		if ($i->{frozen}) {
			&unfreeze_hold($i->{id});
		} else {
			printf "WARN: %s is already unfrozen\n", $i->{id};
		}
	}

}

##############################################################################

# get access_token and set default headers with Authorization
&get_access_token;

if ($freeze) {
	&freeze_patron_holds($base_url . '/v2/patrons/' . $patron . '/holds');
} else {
	&unfreeze_patron_holds($base_url . '/v2/patrons/' . $patron . '/holds');
}
